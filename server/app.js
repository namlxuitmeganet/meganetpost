const koa = require('koa');
const koaRouter = require('koa-router');
const koaBody = require('koa-bodyparser');
const { graphqlKoa } = require('apollo-server-koa');
const { graphiqlKoa } = require('apollo-server-koa');
const mongoose = require('mongoose');
const cors = require('koa2-cors');

const schema = require('./schema/schema');

const app = new koa();
const router = new koaRouter();
const PORT = 4000;

// connect to mlab database
mongoose.connect('mongodb://meganetteam:meganet123@ds117681.mlab.com:17681/meganetpost');
mongoose.connection.once('open', () => {
    console.log('Connected to database.');
});

// allow cross-origin request
app.use(cors());

// koaBody is needed just for POST.
app.use(koaBody());

router.post('/graphql', graphqlKoa({ schema }));
router.get('/graphql', graphqlKoa({ schema }));

// Setup the /graphiql route to show the GraphiQL UI
router.get(
    '/graphiql',
    graphiqlKoa({
        endpointURL: '/graphql', // a POST endpoint that GraphiQL will make the actual requests to
        passHeader: `'Authorization': 'Bearer lorem ipsum'`,
    }),
);

app.use(router.routes());
app.use(router.allowedMethods());
app.listen(PORT, () => {
    console.log(`Server run on http://localhost:${PORT}/graphiql`);
});
