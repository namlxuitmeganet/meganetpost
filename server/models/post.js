const mongoose = require('mongoose');

const PostSchema = new mongoose.Schema({
    title: {
        type: String,
        require: true
    },
    body: {
        type: String,
        require: true
    },
    hashtags: {
        type: String,
        require: true
    },
    published: {
        type: Boolean,
        require: true,
        defaul: true
    },
    userId: {
        type: String,
        require: true
    },
    // comments: {
    //     type: [String],
    //     default: []
    // }
});

module.exports = mongoose.model('Post', PostSchema);