const mongoose = require('mongoose');

const CommentSchema = new mongoose.Schema({
    message: {
        type: String,
        require: true
    },
    userId: {
        type: String,
        require: true
    },
    postId: {
        type: String,
        require: true
    }
});

module.exports = mongoose.model('Comment', CommentSchema);