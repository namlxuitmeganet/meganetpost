const { composeWithMongoose } = require('graphql-compose-mongoose');
const { schemaComposer } = require('graphql-compose');

const User = require('../models/user');
const Post = require('../models/post');
const Comment = require('../models/comment');

const customizationOptions = {};

const UserTC = composeWithMongoose(User, customizationOptions);
const PostTC = composeWithMongoose(Post, customizationOptions);
const CommentTC = composeWithMongoose(Comment, customizationOptions)

UserTC.addRelation(
    'posts',
    {
        resolver: () => PostTC.getResolver('findMany'),
        prepareArgs: {
            filter: (source) => ({
                userId: source._id
            })
        },
        projection: { userId: 1 }
    }
);

UserTC.addRelation(
    'comments',
    {
        resolver: () => CommentTC.getResolver('findMany'),
        prepareArgs: {
            filter: (source) => ({
                userId: source._id
            })
        }
    }
);

PostTC.addRelation(
    'userId',
    {
        resolver: () => UserTC.getResolver('findById'),
        prepareArgs: {
            _id: (source) => source.userId
        },
        projection: { _id: 1 }
    }
);

PostTC.addRelation(
    'comments',
    {
        resolver: () => CommentTC.getResolver('findMany'),
        prepareArgs: {
            filter: (source) => ({
                postId: source._id
            })
        }
    }
);

CommentTC.addRelation(
    'userId',
    {
        resolver: () => UserTC.getResolver('findById'),
        prepareArgs: {
            _id: (source) => source.userId
        },
        projection: { _id: 1 }
    }
);

CommentTC.addRelation(
    'postId',
    {
        resolver: () => PostTC.getResolver('findById'),
        prepareArgs: {
            _id: (source) => source.postId
        },
        projection: { _id: 1 }
    }
);

schemaComposer.rootQuery().addFields({
    userById: UserTC.getResolver('findById'),
    userOne: UserTC.getResolver('findOne'),
    userMany: UserTC.getResolver('findMany'),
    postById: PostTC.getResolver('findById'),
    postMany: PostTC.getResolver('findMany'),
    commentById: CommentTC.getResolver('findById'),
    commentMany: CommentTC.getResolver('findMany')
});

schemaComposer.rootMutation().addFields({
    userCreate: UserTC.getResolver('createOne'),
    userUpdateById: UserTC.getResolver('updateById'),
    userRemoveById: UserTC.getResolver('removeById'),
    postCreate: PostTC.getResolver('createOne'),
    postUpdateById: PostTC.getResolver('updateById'),
    postRemoveById: PostTC.getResolver('removeById'),
    commentCreate: CommentTC.getResolver('createOne'),
    commentUpdateById: CommentTC.getResolver('updateById'),
    commentRemoveById: CommentTC.getResolver('removeById'),
});

const graphqlSchema = schemaComposer.buildSchema();

module.exports = graphqlSchema;