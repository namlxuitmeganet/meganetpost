function formatBody(string, limit = 300) {
    if (string.length > limit) {
        for (let i = limit; i < string.length; i++) {
            if (string[i] === " ") {
                return string.slice(0, i) + "...";
            }
        }
    }

    return string + "...";
}

export { formatBody }