import React from "react";
import { Route, Switch } from "react-router-dom";

import Home from "./components/Home/Home";
import MyPost from "./components/MyPost/MyPost";
import PostDetail from "./components/PostDetail/PostDetail";
import UserPost from "./components/UserPost/UserPost";
import Login from "./components/Login/Login";
import SignUp from "./components/SignUp/SignUp";
import NotFound from "./components/NotFound/NotFound";
import Profile from "./components/Profile/Profile"
import Logout from "./components/Logout/Logout"
import SearchPost from "./components/PostSearch/SearchPost";

export default () =>
  <Switch>
    <Route path="/login" exact component={Login} />
    <Route path="/" exact component={Home} />
    <Route path="/mypost" exact component={MyPost} />
    <Route path="/postDetail/:postId" exact component={PostDetail} />
    <Route path="/userId/:userId" exact component={UserPost} />
    <Route path="/signup" exact component={SignUp} />
    <Route path="/profile" exact component={Profile} />
    <Route path="/logout" exact component={Logout} />
    <Route path="/search/:hashtags" exact component={SearchPost} />
    <Route component={NotFound} />
  </Switch>;