import React, { Component } from 'react';
import { Grid, Row, Col } from "react-bootstrap";
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';

import Routes from "./Routes";
import Navigation from "./components/Navigation/Navigation";
import './App.css';


// apollo client setup
const client = new ApolloClient({
  uri: 'http://localhost:4000/graphql'
});

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }
 
  render() {
    return (
      <ApolloProvider client={client}>
        <div className="App">
          <Navigation />
          <Grid>
            <Row>
              <Col md={8} mdOffset={2}>
                <Routes />
              </Col>
            </Row>
          </Grid>
        </div>
      </ApolloProvider>
    )
  }
}


