import { gql } from 'apollo-boost';

const getPostsQuery = gql`
{
    postMany(
        filter: {
            published: true
        },
        sort: _ID_DESC
    ) {
        title
        _id
        body
        hashtags
        userId {
            _id
            name
        }
    }
}
`;

const getPostQuery = gql`
query GetPost($id: MongoID!) {
    postById(_id: $id) {
        _id
        title
        body
        hashtags
        published
        userId {
            _id
            name
            posts {
            title
            _id
            }
        }
    }
}
`;

const getCommentQuery = gql`
query GetCommentPost($id: String!) {
    commentMany(sort: _ID_DESC, filter: {
        postId: $id
    }) {
        _id
        message
        userId {
            _id
            name
        }
    }
}
`;

const getMyPostQuery = gql`
query GetMyPost($userId: String!) {
    postMany(
        filter: {
          userId: $userId
      },
      sort: _ID_DESC
    ) {
        title
        _id
        body
        hashtags
        published

    }
  }
`;

const getUserQuery = gql`
query GetUser($username: String, $password: String) {
    userOne(filter: {
        username: $username
        password: $password
    }) {
        _id
        name
        username
        email
    }
}
`;
const getUserPostQuery = gql`
query getUserPost($userId: String!) {
    postMany(
        filter: {
            userId: $userId
            published: true
        },
        sort: _ID_DESC
    ) {
        userId {
            _id
            name
        }
        title
        _id
        body
        hashtags
    }
}
`;
const createPostMutation = gql`
mutation createPost($title: String!, $body: String!, $published: Boolean, $userId: String!, $hashtags: String) {
    postCreate(record:  {
        title:$title,
        body:$body,
        hashtags:$hashtags
        published:$published
        userId:$userId
    }) {
        recordId
    }
}
`
//Nam meganet 28.06.2017
const createUserMutation = gql`
mutation createUser($name: String, $username: String, $email: String, $password: String) {
    userCreate(record: {
        name: $name
        username: $username
        email: $email
        password: $password
      }) {
        recordId
        record {
            name
            username
            email
        }
    }
}
`

//post comment
const createCommentMutation = gql`
mutation createComment($message: String, $userId: String,$postId:String) {
    commentCreate(record:{
        message:$message,
        userId:$userId,
        postId:$postId
      }){
        recordId
      }
}
`
const editPostMutation = gql`
mutation editPost($title: String!, $body: String!,$hashtags:String, $published: Boolean!, $postId: MongoID!) {
    postUpdateById(record: {
        title: $title
        body: $body
        hashtags:$hashtags
        published: $published
        _id: $postId
    }) {
        recordId
    }
}
`;
const deletePostMutation = gql`
mutation deletePost($postId: MongoID!) {
    postRemoveById(_id: $postId) {
        recordId
    }
}
`;

const updateUserMutation = gql`
mutation updateUser($id: MongoID!, $name: String!, $email: String!, $username: String!, $password: String!) {
    userUpdateById(record: {
      _id: $id
      name: $name
      email: $email
      username: $username
      password: $password
    }) {
      recordId
      record {
        name
        email
        username
      }
    }
  }
`;

const getSearchPostQuery = gql`
query GetSearchPost($hashtags: String!) {
    postMany(
        filter: {
          hashtags: $hashtags
      },
      sort: _ID_DESC
    ) {
        title
        _id
        body
        hashtags
        published
        userId {
            _id
            name
            posts {
            title
            _id
            }
        }
    }
  }
`;

export { getPostsQuery, getPostQuery, getCommentQuery, getUserQuery, createPostMutation, createUserMutation, createCommentMutation, getMyPostQuery, deletePostMutation, editPostMutation, getUserPostQuery, updateUserMutation, getSearchPostQuery };







