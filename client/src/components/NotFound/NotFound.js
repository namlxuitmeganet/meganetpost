import React from "react";

export default () =>
  <div className="text-center">
    <h1>Sorry, page not found!</h1>
  </div>;