import React, { Component } from "react";
import { graphql } from 'react-apollo';
import { Panel } from 'react-bootstrap';

import { getPostsQuery } from '../../queries/queries';
import { formatBody } from '../../utils';

class Home extends Component {

    displayPost() {
        const { postMany, loading, error } = this.props.data;

        if (loading) {
            return (<h2>Loading posts...</h2>);
        }
        else if (error) {
            return (<div>{error}</div>);
        }
        else {
            return postMany.map((item) =>
                <Panel key={item._id}>
                    <Panel.Heading>
                        <a className="panel-title" href={'/postDetail/' + item._id}>  {item.title}</a>
                    </Panel.Heading>
                    <Panel.Body>
                        <p>{formatBody(item.body)} <a href={'/postDetail/' + item._id}>read more</a></p>
                        <br />
                        <a href={'/Search/' + item.hashtags}>#{item.hashtags}</a>
                    </Panel.Body>
                    <Panel.Footer className="text-right">
                        <a href={"/userId/" + item.userId._id}>{item.userId.name}</a>
                    </Panel.Footer>
                </Panel>
            )
        }
    }
    render() {
        return (
            <div>
                {this.displayPost()}
            </div>
        )
    }
}

export default graphql(getPostsQuery)(Home);