import React, { Component } from "react";
import { graphql } from 'react-apollo';
import { Panel } from 'react-bootstrap';

import { getUserPostQuery } from '../../queries/queries';
import { formatBody } from '../../utils';

class UserPost extends Component {
    displayPost() {
        const { postMany, loading, error } = this.props.data;

        if (loading) {
            return (<h2>Loading posts...</h2>);
        } else if (error) {
            return (<div>{error}</div>);
        } else {
            return (
                <div>
                    <div className="user-info">
                        <img alt="logo meganet" className="user-avatar" src={require('../../assists/images/default-avatar.jpg')} />
                        <h2>{postMany[0].userId.name}</h2>
                    </div>
                    {
                        postMany.map((item) =>
                            <Panel key={item._id}>
                                <Panel.Heading>
                                    <a className="panel-title" href={'/postDetail/' + item._id}>{item.title}</a>
                                </Panel.Heading>
                                <Panel.Body>
                                    <p>{formatBody(item.body)} <a href={'/postDetail/' + item._id}>read more</a></p>
                                </Panel.Body>
                            </Panel>
                        )
                    }
                </div>
            )
        }
    }

    render() {
        const currentUser = JSON.parse(localStorage.getItem("user"));

        if (this.props.match.params.userId === currentUser.id) {
            this.props.history.push("/mypost");
        }
        return (
            <div>
                {this.displayPost()}
            </div>
        )
    }
}

export default graphql(getUserPostQuery, {
    options: (props) => {
        return {
            variables: {
                userId: props.match.params.userId
            }
        }
    }
})(UserPost);