import React, { Component } from "react";
import { withApollo } from 'react-apollo';
import { Panel, Row, Grid, Col, Button, Modal, FormGroup, ControlLabel, FormControl, Checkbox } from 'react-bootstrap';

import { getMyPostQuery, deletePostMutation, editPostMutation } from '../../queries/queries';
import { formatBody } from '../../utils';

class MyPost extends Component {
    constructor(props) {
        super(props);

        this.state = {
            postMany: [],
            loading: true,
            _id: '',
            title: '',
            body: '',
            hashtags: '',
            published: true,
            show: false
        }

        this.handleClose = this.handleClose.bind(this);
        this.handleShow = this.handleShow.bind(this);
    }

    componentWillMount() {
        this.getPost();
    }

    handleClose() {
        this.setState({ show: false });
    }

    handleShow() {
        this.setState({ show: true });
    }

    handlePublished = event => {
        this.setState({ published: event.target.checked });
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        })
    }

    getPost = async () => {
        await this.props.client.query({
            query: getMyPostQuery,
            variables: {
                userId: JSON.parse(localStorage.getItem("user")).id
            }
        }).then((result) => {
            this.setState({
                postMany: result.data.postMany,
                loading: result.loading
            });
        })
    }

    deletePost = async (postId) => {
        await this.props.client.mutate({
            mutation: deletePostMutation,
            variables: {
                postId
            }
        }).then(() => {
            this.props.client.resetStore().then(() => {
                this.getPost();
            })
        })
    }

    getDetail = async (post) => {
        this.setState({
            _id: post._id,
            title: post.title,
            body: post.body,
            hashtags: post.hashtags,
            published: post.published,
            show: true
        })
    }

    handleEdit = async () => {
        const { title, body, hashtags, published, _id } = this.state;

        await this.props.client.mutate({
            mutation: editPostMutation,
            variables: {
                title,
                body,
                published,
                hashtags,
                postId: _id
            }
        }).then(() => {
            this.props.client.resetStore().then(() => {
                this.getPost();
            })

            this.setState({ show: false });
        })
    }

    displayPost() {
        const { postMany, loading } = this.state;

        if (loading) {
            return (<h2>Loading posts...</h2>);
        }
        else {
            return (
                <div>
                    {
                        postMany.map((item) =>
                            <Panel key={item._id}>
                                <Panel.Heading>
                                    <a className="panel-title" href={'/postDetail/' + item._id}>{item.title}</a>
                                </Panel.Heading>
                                <Panel.Body>
                                    <p>{formatBody(item.body)} <a href={'/postDetail/' + item._id}>read more</a></p>
                                    <br />
                                    <a >#{item.hashtags}</a>
                                </Panel.Body>
                                <Panel.Footer className="text-right">
                                    <Button bsStyle="primary" style={{marginRight:10}} bsSize="small" onClick={() => this.getDetail(item)}>EDIT</Button>
                                    <Button bsStyle="danger" bsSize="small" onClick={() => this.deletePost(item._id)}>DELETE</Button>
                                </Panel.Footer>
                            </Panel>
                        )
                    }
                </div>
            )
        }
    }

    render() {
        const { title, body, hashtags, published } = this.state;

        return (
            <div>
                <div className="user-info">
                    <img alt="logo meganet" className="user-avatar" src={require('../../assists/images/default-avatar.jpg')} />
                    <h1>My Post</h1>
                </div>
                {this.displayPost()}
                <Modal show={this.state.show} onHide={this.handleClose} bsSize="large">
                    <Modal.Header>
                        <Modal.Title>Edit post</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <FormGroup controlId="title" bsSize="large">
                            <ControlLabel>Title</ ControlLabel>
                            <FormControl
                                autoFocus
                                type="text"
                                value={title}
                                placeholder="Input title"
                                onChange={this.handleChange}
                                required={true}
                            />
                        </FormGroup>
                        <FormGroup controlId="body" bsSize="large">
                            <ControlLabel>Body</ControlLabel>
                            <FormControl
                                componentClass="textarea"
                                rows={10}
                                value={body}
                                placeholder="Input body"
                                onChange={this.handleChange}
                                required={true}
                            />
                        </FormGroup>
                        <FormGroup controlId="hashtags" bsSize="large">
                            <ControlLabel>Hashtags</ ControlLabel>
                            <FormControl
                                autoFocus
                                type="text"
                                value={hashtags}
                                placeholder="Input hashtags"
                                onChange={this.handleChange}
                                required={true}
                            />
                        </FormGroup>
                    </Modal.Body>
                    <Modal.Footer>
                        <Grid>
                            <Row>
                                <Col md={3} className="text-left">
                                    <Checkbox checked={published} onChange={this.handlePublished}>published</Checkbox>
                                </Col>
                                <Col md={6}>
                                    <Button onClick={this.handleClose}>Close</Button>
                                    <Button bsStyle="primary" onClick={this.handleEdit}>Edit</Button>
                                </Col>
                            </Row>
                        </Grid>
                    </Modal.Footer>
                </Modal>
            </div >
        )
    }
}

export default withApollo(MyPost);