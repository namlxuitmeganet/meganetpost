import React, { Component } from "react";
import { Button, FormGroup, FormControl, ControlLabel, Alert, Grid, Row, Col } from "react-bootstrap";
import { getUserQuery } from "../../queries/queries";
import { withApollo } from "react-apollo";

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: 'A.Nguyen',
            password: '123456',
            success: true
        };
    }

    validateForm() {
        return this.state.username.length > 0 && this.state.password.length > 0;
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    }

    //Submit Form
    handleSubmit = async event => {
        event.preventDefault();

        const { username, password } = this.state;
        const result = await this.props.client.query({
            query: getUserQuery,
            variables: {
                username,
                password
            }
        })
        const data = result.data.userOne;
        if (data) {
            localStorage.setItem("user", JSON.stringify({ "id": data._id, "name": data.name, "username": data.username, "email": data.email }));
            this.props.history.push('/');
        } else {
            this.setState({
                success: false
            })
        }
    }

    render() {
        const { username, password, success } = this.state;
        return (
            <Grid>
                <Row>
                    <Col md={4} mdOffset={2}>
                        <form onSubmit={this.handleSubmit}>
                            <h1>Login to your account</h1>

                            {!success && <Alert bsStyle="danger">Username or password is incorrect!</Alert>}

                            <FormGroup controlId="username" bsSize="large">
                                <ControlLabel>Username</ControlLabel>
                                <FormControl
                                    autoFocus
                                    type="text"
                                    value={username}
                                    onChange={this.handleChange}
                                    required="true"
                                />
                            </FormGroup>
                            <FormGroup controlId="password" bsSize="large">
                                <ControlLabel>Password</ControlLabel>
                                <FormControl
                                    value={password}
                                    onChange={this.handleChange}
                                    type="password"
                                    required="true"
                                />
                            </FormGroup>
                            <Button bsStyle="primary" block bsSize="large" type="submit" disabled={!this.validateForm()}>Login </Button>
                        </form>
                    </Col>
                </Row>
            </Grid>
        );
    }
}

export default withApollo(Login);
