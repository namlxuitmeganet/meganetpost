import React, { Component } from "react";
import Home from "../Home/Home";
export default class Logout extends Component {
    componentWillMount() {
        this.signOut();
    }
    signOut() {
        localStorage.removeItem("user");
        this.props.history.push("/");
    }
    render() {
        return <Home></Home>
    }
}