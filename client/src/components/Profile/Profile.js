import React, { Component } from "react";
import { Button, FormGroup, FormControl, ControlLabel, Grid, Row, Col } from "react-bootstrap";

import "./Profile.css";
import { updateUserMutation } from "../../queries/queries";
import { withApollo } from "react-apollo";

class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userId: "",
            name: "",
            email: "",
            username: "",
            password: "",
            success: false,
            onChange: false
        }
    }

    componentWillMount() {
        const user = JSON.parse(localStorage.getItem("user"));

        this.setState({
            userId: user.id,
            name: user.name,
            email: user.email,
            username: user.username
        });
    }

    //handle submit
    handleSubmit = async (event) => {
        event.preventDefault();

        const { userId, name, username, email, password } = this.state;

        await this.props.client.mutate({
            mutation: updateUserMutation,
            variables: {
                id: userId,
                name,
                username,
                email,
                password
            }
        })
            .then((result) => {
                const data = result.data.userUpdateById;

                localStorage.removeItem("user");
                localStorage.setItem("user", JSON.stringify({ "id": data.recordId, "name": data.record.name, "username": data.record.username, "email": data.record.email }));

                this.setState({
                    password: ""
                });

                alert("Updated successfully");
            })
            .catch((err) => { console.log(err) })
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    }

    render() {
        return (
            <Grid>
                <Row>
                    <Col md={4} mdOffset={2}>
                        <form onSubmit={this.handleSubmit}>
                            <h1>Your Profile</h1>
                            <FormGroup controlId="name" bsSize="large">
                                <ControlLabel>Full Name</ControlLabel>
                                <FormControl
                                    autoFocus
                                    type="text"
                                    value={this.state.name}
                                    onChange={this.handleChange}
                                    required="true"
                                />
                            </FormGroup>
                            <FormGroup controlId="email" bsSize="large">
                                <ControlLabel>Email</ControlLabel>
                                <FormControl
                                    type="email"
                                    value={this.state.email}
                                    onChange={this.handleChange}
                                    required="true"
                                />
                            </FormGroup>
                            <FormGroup controlId="username" bsSize="large">
                                <ControlLabel>User Name</ControlLabel>
                                <FormControl
                                    type="text"
                                    value={this.state.username}
                                    onChange={this.handleChange}
                                    disabled="true"
                                    required="true"
                                />
                            </FormGroup>
                            <FormGroup controlId="password" bsSize="large">
                                <ControlLabel>Password</ControlLabel>
                                <FormControl
                                    value={this.state.password}
                                    onChange={this.handleChange}
                                    type="password"
                                    required="true"
                                />
                            </FormGroup>
                            <Button bsStyle="primary" block bsSize="large" disabled={this.state.onChange} type="submit">Update</Button>
                        </form>
                    </Col>
                </Row>
            </Grid>
        )
    }
}

export default withApollo(Profile);