import React, { Component } from "react";
import { Button, FormGroup, FormControl, ControlLabel, Grid, Row, Col } from "react-bootstrap";
import { withApollo } from "react-apollo"

import { createUserMutation } from "../../queries/queries";

class SignUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            email: "",
            username: "",
            password: "",
            success: false
        }
    }

    validateForm() {
        return this.state.email.length > 0 && this.state.password.length > 0;
    }

    //handle submit
    handleSubmit = async (event) => {
        event.preventDefault();
        const { name, username, email, password } = this.state;
        const result = await this.props.client.mutate({
            mutation: createUserMutation,
            variables: {
                name,
                username,
                email,
                password
            }
        }).catch((err) => { console.log(err) })
        const data = result.data.userCreate;
        if (data) {
            localStorage.setItem("user", JSON.stringify({ "id": data.recordId, "name": data.record.name, "username": data.record.username, "email": data.record.email }));
            this.props.history.push("/");
        }
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    }

    render() {
        return (
            <Grid>
                <Row>
                    <Col md={4} mdOffset={2}>
                        <form onSubmit={this.handleSubmit}>
                            <h1>SignUp to your Account</h1>
                            <FormGroup controlId="name" bsSize="large">
                                <ControlLabel>Full Name</ControlLabel>
                                <FormControl
                                    autoFocus
                                    type="text"
                                    value={this.state.name}
                                    onChange={this.handleChange}
                                    required="true"
                                />
                            </FormGroup>
                            <FormGroup controlId="email" bsSize="large">
                                <ControlLabel>Email</ControlLabel>
                                <FormControl
                                    type="email"
                                    value={this.state.email}
                                    onChange={this.handleChange}
                                    required="true"
                                />
                            </FormGroup>
                            <FormGroup controlId="username" bsSize="large">
                                <ControlLabel>User Name</ControlLabel>
                                <FormControl
                                    type="text"
                                    value={this.state.username}
                                    onChange={this.handleChange}
                                    required="true"
                                />
                            </FormGroup>
                            <FormGroup controlId="password" bsSize="large">
                                <ControlLabel>Password</ControlLabel>
                                <FormControl
                                    value={this.state.password}
                                    onChange={this.handleChange}
                                    type="password"
                                    required="true"
                                />
                            </FormGroup>
                            <Button bsStyle="primary" block bsSize="large" disabled={!this.validateForm()} type="submit">SignUp</Button>
                        </form>
                    </Col>
                </Row>
            </Grid>
        )
    }
}
export default withApollo(SignUp);