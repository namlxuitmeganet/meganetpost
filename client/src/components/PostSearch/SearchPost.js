import React, { Component } from "react";
import { withApollo } from 'react-apollo';
import { Panel } from 'react-bootstrap';

import { getSearchPostQuery } from '../../queries/queries';
import { formatBody } from '../../utils';

class SearchPost extends Component {

    constructor(props) {
        super(props);
        this.state = {
            postMany: [],
            loading: true,
            _id: '',
            title: '',
            body: '',
            hashtags: '',
            published: true,
        }
    }

    componentWillMount() {
        this.getPost();
    }
    getPost = async () => {
        await this.props.client.query({
            query: getSearchPostQuery,
            variables: {
                hashtags: this.props.match.params.hashtags
            }
        }).then((result) => {
            this.setState({
                postMany: result.data.postMany,
                loading: result.loading
            });
        })
    }
    displayPost() {
        const { postMany, loading } = this.state;

        if (loading) {
            return (<h2>Loading posts...</h2>);
        }
        else {
            return (
                <div>
                    <div className="user-info">
                        <h1>Search Page</h1>
                    </div>
                    {
                        postMany.map((item) =>
                            <Panel key={item._id}>
                                <Panel.Heading>
                                    <a className="panel-title" href={'/postDetail/' + item._id}>{item.title}</a>
                                </Panel.Heading>
                                <Panel.Body>
                                    <p>{formatBody(item.body)} <a href={'/postDetail/' + item._id}>read more</a></p>
                                    <br />
                                    <a >#{item.hashtags}</a>
                                </Panel.Body>
                                <Panel.Footer className="text-right">
                                    <a href={"/userId/" + item.userId._id}>{item.userId.name}</a>
                                </Panel.Footer>
                            </Panel>
                        )
                    }
                </div>
            )
        }
    }
    render() {
        return (
            <div>
                {this.displayPost()}
            </div>
        )
    }
}

export default withApollo(SearchPost);