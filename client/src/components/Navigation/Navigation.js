import React, { Component } from 'react';
import { Navbar, Nav, NavItem, Modal, NavDropdown, MenuItem, Button, FormControl, FormGroup, ControlLabel, Checkbox, Grid, Row, Col } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import { withApollo } from "react-apollo";

import { createPostMutation } from '../../queries/queries';

class Navigation extends Component {
    constructor(props) {
        super(props);
        this.handleShow = this.handleShow.bind(this);
        this.handleClose = this.handleClose.bind(this);

        this.state = {
            show: false,
            title: '',
            body: '',
            hashtags: '',
            published: true,
            userId: ''
        };
    }

    componentWillMount() {
        const currentUser = JSON.parse(localStorage.getItem("user"));

        if (currentUser) {
            this.setState({ userId: currentUser.id })
        }
    }

    handleClose() {
        this.setState({ show: false });
    }

    handleShow() {
        this.setState({ show: true });
    }

    handlePublished = event => {
        this.setState({ published: event.target.checked });

    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    }

    //handle Post
    handlePost = async (event) => {
        event.preventDefault();
        const { title, body, published, hashtags, userId } = this.state;
        await this.props.client.mutate({
            mutation: createPostMutation,
            variables: {
                title,
                body,
                hashtags,
                published,
                userId
            }
        })
            .then(() => {
                this.setState({ show: false })

                alert("Posted successfully!");
                window.location.reload();
            })
            .catch((err) => { console.log(err) })
    }

    render() {
        const { title, body, published, hashtags } = this.state;

        const currentUser = JSON.parse(localStorage.getItem("user"));

        const guestLinks = (
            <Navbar.Collapse>
                <Nav pullRight>
                    <LinkContainer to="/signup">
                        <NavItem>SignUp</NavItem>
                    </LinkContainer>
                    <LinkContainer to="/Login">
                        <NavItem>Login</NavItem>
                    </LinkContainer>
                </Nav>
            </Navbar.Collapse>
        )

        const userLinks = (
            <Navbar.Collapse>
                <Nav pullRight>
                    <NavItem onClick={this.handleShow}>Create Post</NavItem>
                    <NavDropdown eventKey={3} title={currentUser ? "Hello, " + currentUser.name : ""} id="basic-nav-dropdown">
                        <LinkContainer to="/profile" >
                            <MenuItem eventKey={3.1}>Profile</MenuItem>
                        </LinkContainer>
                        <LinkContainer to="/mypost" >
                            <MenuItem eventKey={3.2}>My Post</MenuItem>
                        </LinkContainer>
                        <MenuItem divider />
                        <LinkContainer to="/Logout" >
                            <MenuItem eventKey={3.3}>Logout</MenuItem>
                        </LinkContainer>
                    </NavDropdown>
                </Nav>
            </Navbar.Collapse>
        )

        return (
            <div>
                <Navbar inverse collapseOnSelect>
                    <Navbar.Header>
                        <Navbar.Toggle />
                        <a href="/">
                            <img alt="logo meganet" className="logo" src={require('../../assists/images/logo.png')}></img>
                        </a>
                    </Navbar.Header>
                    {currentUser ? userLinks : guestLinks}
                </Navbar>
                <Modal show={this.state.show} onHide={this.handleClose} bsSize="large">
                    <Modal.Header>
                        <Modal.Title>Create a new post</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <FormGroup controlId="title" bsSize="large">
                            <ControlLabel>Title</ ControlLabel>
                            <FormControl
                                autoFocus
                                type="text"
                                value={title}
                                placeholder="Input title"
                                onChange={this.handleChange}
                                required={true}
                            />
                        </FormGroup>
                        <FormGroup controlId="body" bsSize="large">
                            <ControlLabel>Body</ControlLabel>
                            <FormControl
                                componentClass="textarea"
                                rows={10}
                                value={body}
                                placeholder="Input body"
                                onChange={this.handleChange}
                                required={true}
                            />
                        </FormGroup>
                        <FormGroup controlId="hashtags" bsSize="large">
                            <ControlLabel>Hashtags</ ControlLabel>
                            <FormControl
                                autoFocus
                                type="text"
                                value={hashtags}
                                placeholder="Input hashtags"
                                onChange={this.handleChange}
                                required={true}
                            />
                        </FormGroup>
                    </Modal.Body>
                    <Modal.Footer>
                        <Grid>
                            <Row>
                                <Col md={3} className="text-left">
                                    <Checkbox checked={published} onChange={this.handlePublished}>published</Checkbox>
                                </Col>
                                <Col md={6}>
                                    <Button onClick={this.handleClose}>Close</Button>
                                    <Button bsStyle="primary" onClick={this.handlePost}>Post</Button>
                                </Col>
                            </Row>
                        </Grid>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}

export default withApollo(Navigation);