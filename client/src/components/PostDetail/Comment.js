import React, { Component } from 'react'
import { withApollo } from 'react-apollo';
import { FormGroup, FormControl, ListGroup, ListGroupItem } from 'react-bootstrap';

import { createCommentMutation, getCommentQuery } from '../../queries/queries';

class Comment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            comments: [],
            message: "",
            userId: "",
            postId: "",
            loading: true
        }
    }

    componentWillMount() {
        this.getComment();
    }

    getComment = async () => {
        await this.props.client.query({
            query: getCommentQuery,
            variables: {
                id: this.props.postId
            }
        })
            .then((result) => {
                this.setState({
                    comments: result.data.commentMany,
                    loading: result.loading
                });
            })
            .catch((err) => { console.log(err) })
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    }

    handleSubmit = async event => {
        event.preventDefault();

        const currentUser = JSON.parse(localStorage.getItem("user"));

        const { message } = this.state;
        await this.props.client.mutate({
            mutation: createCommentMutation,
            variables: {
                message,
                userId: currentUser.id,
                postId: this.props.postId
            }
        })
            .then(() => {
                this.props.client.resetStore().then(() => {
                    this.getComment();
                })

                this.setState({ message: "" });
            })
            .catch((err) => { console.log(err) })
    }

    render() {
        const { message, comments, loading } = this.state;

        if(loading) {
            return (<h2>Loading comments...</h2>)
        }

        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <FormGroup controlId="message" bsSize="lg" >
                        <FormControl
                            autoFocus
                            type="text"
                            value={message}
                            placeholder="Comment"
                            onChange={this.handleChange}
                            required={true}
                        />
                    </FormGroup>
                </form>
                <ListGroup>
                    {
                        comments.map((item) =>
                            <div key={item._id}>
                                <ListGroupItem><a href={"/userId/" + item.userId._id}>{item.userId.name}</a>: {item.message}</ListGroupItem>
                                <br />
                            </div>
                        )
                    }
                </ListGroup>
            </div>
        )
    }
}
export default withApollo(Comment)