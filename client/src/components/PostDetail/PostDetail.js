import React, { Component } from "react";
import { graphql } from 'react-apollo';
import { Jumbotron } from 'react-bootstrap';

import Comment from "./Comment";
import { getPostQuery } from "../../queries/queries"

class PostDetail extends Component {
    displayPost() {
        const currentUser = JSON.parse(localStorage.getItem("user"));
        const { postById, loading, error } = this.props.data;

        if (loading) {
            return (<h2>Loading post...</h2>);
        }
        else if (error) {
            return (<div>{error}</div>)
        }
        else {
            return (
                <Jumbotron>
                    <h2>{postById.title}</h2>
                    <div className="text-center">
                        <a href={"/userId/" + postById.userId._id}>{postById.userId.name}</a>
                    </div>
                    <hr />
                    <p>{postById.body}</p>
                    <hr />

                    {currentUser ? <Comment postId={this.props.data.postById._id} /> : null}
                </Jumbotron>
            );
        }
    }
    render() {

        return (
            <div>
                {this.displayPost()}
            </div>
        )
    }
}

export default graphql(getPostQuery, {
    options: (props) => {
        return {
            variables: {
                id: props.match.params.postId
            }
        }
    }
})(PostDetail);
