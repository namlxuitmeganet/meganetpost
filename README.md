# Member: 
* Lê Xuân Nam
* Phạm Dũ Phong
* Đinh Công Đạt
* Nguyễn Ngọc Vương
* Nguyễn Thanh Danh

# Company: NIGHTSTAR SOFTWARE TECHNOLOGY .,JSC
# Working
# Task:
# DB:
```
User(id, username, name, email, password)
Post(id, title, body, hashtags, published)
Comment(id, message)

Relations:
user has many posts - post belongs to user
post has many comments - comment belongs to post
user has many comments - comment belongs to user
```

# Backend:

Create a Graphql API with [`graphql-compose-moongose`](https://graphql-compose.github.io/docs/en/plugin-mongoose.html)

# Frontend: Create a react app
```
- List all published posts of all users
- User can create a post
- User can publish/unpublish a post
- User can read a published post
- User can comment on a published post
```

# Note: 

No authentication is required yet

[](Elegantt_data:dont_delete{"ignored":false,"autoPlanned":false,"ownerId":false,"dependencies":[]})
